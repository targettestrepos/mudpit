FROM python:3-alpine

RUN pip install flask

COPY app.py /app/app.py

ENV FLASK_APP=app.py
WORKDIR /app

CMD ["flask", "run", "--host=0.0.0.0", "--port=80"]